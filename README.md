# challenge-abstracta

Runs in Linux. Not tested on Windows / Mac but maybe works in those OS.

# Install

1. Clone the repository locally
2. Install Google Chrome
3. Install python
4. Install pip (https://pip.pypa.io/en/stable/installation/)
5. Install pipenv: $ pip install pipenv
6. Run: $ pipenv install

# Run locally

Write the following command:

    $ pipenv run python -m pytest --browser=chrome --html=reports/report-chrome.html --self-contained-html

# Reports and Screenshots

The html reports will be generated in the reports folder,
and will include screenshots that are saved inside reports/screenshot folder.