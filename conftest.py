import pytest, os

from src.utils.reports import delete_reports_in_dir
from src.utils.reports import delete_screenshots_in_dir
from src.utils.reports import report_fail_test
from src.utils.browser_setup import browser_selection


def pytest_addoption(parser):
    parser.addoption(
        "--browser",
        action="store",
        default="chrome",
        choices=("chrome"),
        help="Browser to run the test cases (options: chrome)"
    )
    parser.addoption(
        "--headless",
        action="store_true",
        default=False,
        help="Set headless mode"
    )

@pytest.fixture
def driver_args(request, scope='session'):
    return {
        "browser": request.config.getoption("--browser"),
        "headless": request.config.getoption("--headless")
    }


# Delete old report files
@pytest.fixture(scope='session', autouse=True)
def clean_old_reports(request):
    browser = request.config.getoption("--browser")

    delete_reports_in_dir(end_file=f"{browser}.html")
    delete_screenshots_in_dir(start_file=browser, end_file=".png")


@pytest.fixture
def driver(driver_args):
    # Set Up driver
    driver = browser_selection(driver_args["browser"], driver_args["headless"])
    driver.maximize_window()
    driver.get("https://www.saucedemo.com/")

    yield driver

    driver.quit()


@pytest.hookimpl(hookwrapper=True)
def pytest_runtest_makereport(item):
    html_plugin = item.config.pluginmanager.getplugin("html")

    outcome = yield
    if item.config.option.markexpr == 'api':
        return

    report = outcome.get_result()
    report_fail_test(report, item, html_plugin)
