from selenium import webdriver

from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager


def browser_selection(browser, headless):
    if browser == "chrome":
        driver = webdriver.Chrome(
            service=ChromeService(ChromeDriverManager().install()),
            options=chrome_options(headless)
        )
    elif browser == "firefox":
        driver = None
    elif browser == "edge":
        driver = None
    elif browser == "opera":
        driver = None
    elif browser == "brave":
        driver = None
    elif browser == "chromium":
        driver = None
    return driver


def chrome_options(headless):
    options = webdriver.ChromeOptions()
    if headless:
        options.add_argument("--headless=new")
    return options
