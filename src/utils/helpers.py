import re


def parse_product_id(href):
    return href.split("product_id=")[1].split("&")[0]


def validate_filename(filename):
    if not re.match(r"^\w{3}\w*\.png$", filename):
        raise Exception("Screenshot filename should have at least 3 alphanumeric characters and must end with .png")