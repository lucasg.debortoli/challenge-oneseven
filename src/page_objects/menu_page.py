from src.locators.menu_page_locators import MenuPageLocators
from src.page_objects.base_page import BasePage


class MenuPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)

    def get_cart_item_number_icon(self):
        return int(self.find_element(MenuPageLocators.items_in_cart_icon).text)

    def go_to_next_page(self, click_item_locator, path_page):
        self.find_element(click_item_locator).click()
        assert self.get_page_url().endswith(f"{path_page}.html")

    def go_to_cart_page(self):
        self.go_to_next_page(MenuPageLocators.cart_icon, 'cart')
