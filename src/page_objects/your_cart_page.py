from src.locators.your_cart_page_locators import YourCartPageLocators
from src.page_objects.cart_page import CartPage


class YourCartPage(CartPage):

    def __init__(self, driver):
        super().__init__(driver)

    def remove_non_clothes_from_cart(self):
        remove_backpack = self.return_element_if_present(YourCartPageLocators.remove_backpack)
        if remove_backpack:
            remove_backpack.click()

        remove_bike_light = self.return_element_if_present(YourCartPageLocators.remove_bike_light)
        if remove_bike_light:
            remove_bike_light.click()

        return self.get_cart_items()

    def go_to_checkout_page(self):
        self.go_to_next_page(YourCartPageLocators.checkout, 'checkout-step-one')