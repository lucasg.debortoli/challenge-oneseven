import random

from src.page_objects.menu_page import MenuPage
from src.locators.inventory_page_locators import InventoryPageLocators


class InventoryPage(MenuPage):

    def __init__(self, driver):
        super().__init__(driver)

    def add_all_items_to_cart(self):
        items = self.find_elements(InventoryPageLocators.add_to_cart)
        for item in items:
            item.click()
        return len(items)

    def add_random_items_to_cart(self, amount):
        items = self.find_elements(InventoryPageLocators.add_to_cart)
        if len(items) < amount:
            raise Exception("Not enough items to add")

        selected_items = random.sample(items, amount)
        for item in selected_items:
            item.click()

        return len(selected_items)

    def sort_by_price(self):
        self.find_element(InventoryPageLocators.product_sort_dropdown).click()
        self.find_element(InventoryPageLocators.option_low_to_high).click()
