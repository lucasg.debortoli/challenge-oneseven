from src.locators.checkout_page_locators import CheckoutPageLocators
from src.page_objects.menu_page import MenuPage


class CheckoutPage(MenuPage):

    def __init__(self, driver):
        super().__init__(driver)

    def fulfill_info(self, first_name, last_name, zip_code):
        self.find_element(CheckoutPageLocators.first_name).send_keys(first_name)
        self.find_element(CheckoutPageLocators.last_name).send_keys(last_name)
        self.find_element(CheckoutPageLocators.zip_code).send_keys(zip_code)

    def go_to_overview_page(self):
        self.go_to_next_page(CheckoutPageLocators.continue_button, 'checkout-step-two')
