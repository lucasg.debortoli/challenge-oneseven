from src.locators.overview_page_locators import OverviewPageLocators
from src.page_objects.cart_page import CartPage


class OverviewPage(CartPage):

    def __init__(self, driver):
        super().__init__(driver)

    def get_item_sum_prices(self):
        return float(self.find_element(OverviewPageLocators.total_price).text.split('$')[1])

    def go_to_complete_page(self):
        self.go_to_next_page(OverviewPageLocators.finish, 'checkout-complete')