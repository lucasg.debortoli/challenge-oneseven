from src.page_objects.base_page import BasePage
from src.locators.login_page_locators import LoginPageLocators


class LoginPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)

    def login(self, user, password):
        self.find_element(LoginPageLocators.username).send_keys(user)
        self.find_element(LoginPageLocators.password).send_keys(password)
        self.find_element(LoginPageLocators.login_button).click()

    def get_error_login_message(self):
        return self.find_element(LoginPageLocators.error_login).text
