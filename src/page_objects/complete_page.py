from src.locators.complete_page_locators import CompletePageLocators
from src.page_objects.menu_page import MenuPage


class CompletePage(MenuPage):

    def __init__(self, driver):
        super().__init__(driver)

    def get_order_complete_messages(self):
        return (
            self.find_element(CompletePageLocators.order_complete).text.strip(),
            self.find_element(CompletePageLocators.order_complete_text).text.strip()
        )

    def go_to_inventory_page(self):
        self.go_to_next_page(CompletePageLocators.back_home, 'inventory')
