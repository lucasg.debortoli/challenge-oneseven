from src.locators.cart_page_locators import CartPageLocators
from src.page_objects.menu_page import MenuPage


class CartPage(MenuPage):

    def __init__(self, driver):
        super().__init__(driver)

    def get_cart_items(self):
        web_cart_items = self.find_elements(CartPageLocators.cart_items)
        cart_items = {"elements": {}, "total_price": 0}

        for web_cart_item in web_cart_items:
            item_name = self.find_element(CartPageLocators.cart_item_name, relative_to=web_cart_item).text.strip()
            item_desc = self.find_element(CartPageLocators.cart_item_description,
                                          relative_to=web_cart_item).text.strip()
            item_qty = int(self.find_element(CartPageLocators.cart_quantity, relative_to=web_cart_item).text)
            item_price = float(self.find_element(CartPageLocators.cart_item_price, relative_to=web_cart_item).text[1:])

            cart_items["elements"][item_name] = {
                "name": item_name,
                "description": item_desc,
                "quantity": item_qty,
                "price": item_price
            }
            cart_items["total_price"] += (item_qty * item_price)

        return cart_items
