import os

import pytest

from src.tests.base_test import BaseTest
from src.page_objects.login_page import LoginPage


@pytest.fixture
def login_page(driver):
    login_page = LoginPage(driver)
    yield login_page


@pytest.mark.usefixtures("login_page")
class TestLoginShop(BaseTest):

    def test_login_correct_credentials(self, login_page):
        login_page.login(os.environ["SAUCEDEMO_STANDARD_USERNAME"], os.environ["SAUCEDEMO_STANDARD_PASSWORD"])
        assert login_page.get_page_url().endswith('inventory.html')

    def test_login_wrong_username(self, login_page):
        login_page.login('wrong_username', os.environ["SAUCEDEMO_STANDARD_PASSWORD"])
        assert login_page.get_error_login_message().endswith(
            'Username and password do not match any user in this service'
        )

    def test_login_wrong_password(self, login_page):
        login_page.login(os.environ["SAUCEDEMO_STANDARD_USERNAME"], 'wrong_password')
        assert login_page.get_error_login_message().endswith(
            'Username and password do not match any user in this service'
        )

    def test_login_locked_out_user(self, login_page):
        login_page.login(os.environ["SAUCEDEMO_LOCKED_USERNAME"], os.environ["SAUCEDEMO_LOCKED_PASSWORD"])
        assert login_page.get_error_login_message().endswith('Sorry, this user has been locked out.')
