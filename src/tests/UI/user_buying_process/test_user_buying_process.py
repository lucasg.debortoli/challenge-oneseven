import os

import pytest

from src.page_objects.complete_page import CompletePage
from src.page_objects.overview_page import OverviewPage
from src.page_objects.your_cart_page import YourCartPage
from src.page_objects.checkout_page import CheckoutPage
from src.page_objects.inventory_page import InventoryPage
from src.page_objects.login_page import LoginPage
from src.tests.base_test import BaseTest


@pytest.fixture(autouse=True)
def login(driver):
    login_page = LoginPage(driver)
    login_page.login(os.environ["SAUCEDEMO_STANDARD_USERNAME"], os.environ["SAUCEDEMO_STANDARD_PASSWORD"])
    yield


@pytest.fixture
def inventory_page(driver):
    inventory_page = InventoryPage(driver)
    yield inventory_page


@pytest.fixture
def your_cart_page(driver):
    your_cart_page = YourCartPage(driver)
    yield your_cart_page


@pytest.fixture
def checkout_page(driver):
    checkout_page = CheckoutPage(driver)
    yield checkout_page


@pytest.fixture
def overview_page(driver):
    overview_page = OverviewPage(driver)
    yield overview_page


@pytest.fixture
def complete_page(driver):
    complete_page = CompletePage(driver)
    yield complete_page


@pytest.mark.usefixtures("inventory_page", "your_cart_page", "checkout_page", "overview_page", "complete_page")
class TestUsingBuyingProcess(BaseTest):

    # Scenario 2-1
    def test_buy_clothes(self, inventory_page, your_cart_page, checkout_page, overview_page, complete_page):
        amount = inventory_page.add_all_items_to_cart()
        assert amount == inventory_page.get_cart_item_number_icon()
        self.complete_buy_items_flow(inventory_page, your_cart_page, checkout_page, overview_page, complete_page)

    # Scenario 2-2 and 2-3
    def test_two_random_products(self, inventory_page, your_cart_page, checkout_page, overview_page, complete_page):
        inventory_page.sort_by_price()
        inventory_page.add_random_items_to_cart(2)
        assert 2 == inventory_page.get_cart_item_number_icon()
        self.complete_buy_items_flow(inventory_page, your_cart_page, checkout_page, overview_page, complete_page)

    def complete_buy_items_flow(self, inventory_page, your_cart_page, checkout_page, overview_page, complete_page):
        inventory_page.go_to_cart_page()

        items_in_cart = your_cart_page.remove_non_clothes_from_cart()
        assert len(items_in_cart["elements"]) == your_cart_page.get_cart_item_number_icon()
        your_cart_page.go_to_checkout_page()

        checkout_page.fulfill_info("Lucas", "De Bortoli", "1234567890")
        checkout_page.go_to_overview_page()

        assert items_in_cart["total_price"] == overview_page.get_item_sum_prices()
        items_overview = overview_page.get_cart_items()
        assert len(items_overview["elements"]) == len(items_in_cart["elements"])

        for item in items_overview["elements"]:
            assert item in items_in_cart["elements"]
            assert items_in_cart["elements"][item]["name"] == items_overview["elements"][item]["name"]
            assert items_in_cart["elements"][item]["description"] == items_overview["elements"][item]["description"]
            assert items_in_cart["elements"][item]["quantity"] == items_overview["elements"][item]["quantity"]
            assert items_in_cart["elements"][item]["price"] == items_overview["elements"][item]["price"]

        overview_page.go_to_complete_page()

        messages = complete_page.get_order_complete_messages()
        assert messages[0] == "Thank you for your order!"
        assert messages[1] == "Your order has been dispatched, and will arrive just as fast as the pony can get there!"

        complete_page.go_to_inventory_page()
