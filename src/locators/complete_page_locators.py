from selenium.webdriver.common.by import By


class CompletePageLocators:
    order_complete = By.CLASS_NAME, "complete-header"
    order_complete_text = By.CLASS_NAME, "complete-text"
    back_home = By.ID, "back-to-products"
