from selenium.webdriver.common.by import By


class MenuPageLocators:
    cart_icon = By.CLASS_NAME, "shopping_cart_link"
    items_in_cart_icon = By.CLASS_NAME, "shopping_cart_badge"
