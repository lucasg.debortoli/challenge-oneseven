from selenium.webdriver.common.by import By


class OverviewPageLocators:
    total_price = By.CLASS_NAME, 'summary_subtotal_label'
    finish = By.ID, 'finish'
