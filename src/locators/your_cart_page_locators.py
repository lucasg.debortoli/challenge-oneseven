from selenium.webdriver.common.by import By


class YourCartPageLocators:
    remove_backpack = By.ID, 'remove-sauce-labs-backpack'
    remove_bike_light = By.ID, 'remove-sauce-labs-bike-light'
    checkout = By.ID, 'checkout'
