from selenium.webdriver.common.by import By


class InventoryPageLocators:
    add_to_cart = By.XPATH, "//div[@class=\"inventory_item\"]//button[normalize-space(text())=\"Add to cart\"]"
    product_sort_dropdown = By.CLASS_NAME, "product_sort_container"
    option_low_to_high = By.XPATH, "//option[@value=\"lohi\"]"
