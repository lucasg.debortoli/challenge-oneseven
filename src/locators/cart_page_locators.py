from selenium.webdriver.common.by import By


class CartPageLocators:
    cart_items = By.CLASS_NAME, 'cart_item'
    cart_quantity = By.CLASS_NAME, 'cart_quantity'
    cart_item_name = By.CLASS_NAME, 'inventory_item_name'
    cart_item_description = By.CLASS_NAME, 'inventory_item_desc'
    cart_item_price = By.CLASS_NAME, 'inventory_item_price'
