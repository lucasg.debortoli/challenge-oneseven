from selenium.webdriver.common.by import By


class CheckoutPageLocators:
    first_name = By.ID, 'first-name'
    last_name = By.ID, 'last-name'
    zip_code = By.ID, 'postal-code'
    continue_button = By.ID, 'continue'
